﻿using APIusuario.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace APIusuario
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://senacao.tk/objetos/usuario";

            Usuario usuario = BuscarUsuario(url);

            Console.WriteLine("Usuario");
            Console.WriteLine(
                String.Format("Nome: {0} - Email: {1} - Telefone: {2} - Endereco: {3}",
                usuario.Nome, usuario.Email, usuario.Telefone, usuario.Endereco));
            Console.WriteLine("\n\n");
            Console.WriteLine("Conhecimentos");
            


            
           

            foreach (string Conhecimentos in usuario.Conhecimentos)
            {
                Console.WriteLine(Conhecimentos);
            }

            Console.WriteLine("\n\n");

            foreach (Qualificacao Qualificacao in usuario.Qualificacoes)
            {
                Console.WriteLine(Qualificacao.Nome);
                Console.WriteLine(Qualificacao.Instituicao);
                Console.WriteLine(Qualificacao.Ano);
            }

            Console.ReadLine();
        }
        public static Usuario BuscarUsuario(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(content);

            return usuario;


        }
    }
}
